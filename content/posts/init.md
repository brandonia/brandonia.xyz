---
title: init
date: Sat Dec 3 07:17:21 CDT 2022
tldr: Published my own website! For reasons.
tags:
- infrastructure
author: Brandon
images: /img/keyboard_paw.jpg
---

Website **initialization** complete!!!

This will be a masterlist of every notable change to my domain. (ie. spinning up new vps, recreating the whole site and adding subdomains)

## 12/17/2022
- Installed Heimdall
- Setup 'heimdall.brandonia.xyz' in proxy manager
- Setup SSL on Heimdall

## 12/3/2022
- Built an Nginx Proxy Manager container on a linode
- Setup `brandonia.xyz` in proxy manager
- Setup `dash.brandonia.xyz` in proxy manager
- Setup `npm.brandonia.xyz` in proxy manager
- Setup `up.brandonia.xyz` in proxy manager
- Setup SSL on all these sub domains


## 10/31/2022
- Did a small overhaul of my domains. I condensed down to one registar.
- Migrated over to Linode from DigitalOcean.
- Finished up some ansible roles to automate my website configuration. Still needs some work.
- Setup Cockpit for a dashboard
