---
title: Linux Foundation Certification
description: 
date: Sat Dec 10 04:22:44 CST 2022
tldr: A journey to get my first Linux cert!
draft: true
tags: 
  - LFCS
  - Linux
images:
author: brandon
---
In order to advance my career in the IT field into the direction of DevOps Engineer, I thought a good place to start would be to finally buckle down and get certified in come manner. I want to get a few certs but I want to keep my goals simple and start with one thing at a time. Starting with a entry lever cert like the Linux Foundation can show that I know my way around a Linux system and I'm up to date with current tools.

Below will be a list of links to my notes repo so anyone could see what I learned.

## domains
[Essential Commands](https://gitlab.com/infra_rez/lfcs/-/tree/master/Essential-Commands)

[Networking](https://gitlab.com/infra_rez/lfcs/-/tree/master/Networking)

[Operations and Deployment](https://gitlab.com/infra_rez/lfcs/-/tree/master/Operations-and-Deployment)

[Service](https://gitlab.com/infra_rez/lfcs/-/tree/master/Service)

[Storage](https://gitlab.com/infra_rez/lfcs/-/tree/master/Storage)

[Users and Groups](https://gitlab.com/infra_rez/lfcs/-/tree/master/Users-and-Groups)