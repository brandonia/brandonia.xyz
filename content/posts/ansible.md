---
title: Ansible Roles
date: Wed Nov  2 05:00:00 CDT 2022
tldr: A master list of my ansible roles
tags: 
- ansible
author: Brandon
images: /img/code.jpg
draft: false
---

Ansible is a tool that I found on my journey of exploring my passion for system administration. I feel like it changed my path to become a Dev Ops Engineer. On top of that but it led me into discovering the concept of Infrastructure as Code. I love that my code documents itself. A lazy sys admin is a good sys admin!

I started off with simple playbooks but found that making roles to be the best way to go about dong things. You can encapsulate your intent succinctly and then you can even put roles together into a master playbook.


## roles

Below is a master list of my collection of ansible roles that I use in my infrastructure. Check them out and please leave any type of suggestions!

| Role | Description |
|--|--|
| [common](https://gitlab.com/infra_rez/ansible-common) | Base configuration |
| [dotfiles](https://gitlab.com/infra_rez/ansible-dotfiles) | My Dotfiles |
| [nginx](https://gitlab.com/infra_rez/ansible-nginx) | Nginx role |
| [brandonia.xyz](https://gitlab.com/infra_rez/ansible-brandonia.xyz) | My website setup |
| [window-managers](https://gitlab.com/infra_rez/ansible-window-managers) | Window managers |
| [docker](https://gitlab.com/infra_rez/ansible-docker) | Docker role |
| [nginx-proxy-manager](https://gitlab.com/infra_rez/ansible-nginx-proxy-manager) | NPM Docker container |
| [uptime-kuma](https://gitlab.com/infra_rez/ansible-uptime-kuma) | Uptime Kuma docker container |
| [heimdall](https://gitlab.com/infra_rez/ansible-heimdall) | Heimdall docker container |